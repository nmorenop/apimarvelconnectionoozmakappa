# This is a sample Python script.

# Press Mayús+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import requests
import hashlib

publickey = '7db552d10cbfde93a795dfd2aa18fa18'
privatekey = '0d99d47355cdca6b93be9579efc8e570524d244b'
ts = '1'
h = hashlib.md5((ts + privatekey + publickey).encode()).hexdigest()

base = 'https://gateway.marvel.com/v1/public/'
comics = requests.get(base + 'comics',
                      params={'apikey': publickey,
                              'ts': ts,
                              'hash': h}).json()
characters = requests.get(base + 'characters',
                          params={'apikey': publickey,
                                  'ts': ts,
                                  'hash': h}).json()
wolverine = requests.get(base + 'characters',
                          params={'apikey': publickey,
                                  'ts': ts,
                                  'hash': h,
                                  'name': 'wolverine'}).json()

print(characters)
print("-----------------")
print(wolverine)
